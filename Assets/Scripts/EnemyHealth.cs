﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour
{
	public int maxHealth=100;
	public int curHealth=50;
	public Vector3 screenPosition;
	
	public float healthBarLenght;
	
	// Use this for initialization
	void Start () {
		healthBarLenght=Screen.width/2;
	}
	
	// Update is called once per frame
	void Update () {
		AddjustCurrentHealth(0);
		
	}
	void OnGUI(){
		
		screenPosition = Camera.main.WorldToScreenPoint(transform.position);
		screenPosition.y = Screen.height - screenPosition.y / 1.6f;
		
		GUI.Box(new Rect(screenPosition.x - screenPosition.x / 4, screenPosition.y-40,healthBarLenght,20),curHealth+"/"+maxHealth);
	}
	public void AddjustCurrentHealth(int adj){
		curHealth +=adj;
		
		if(curHealth<1)
			curHealth=0;
		if(curHealth>maxHealth)
			curHealth=maxHealth;
		if(maxHealth<1)
			maxHealth=1;
		healthBarLenght=(Screen.width/2)*(curHealth/(float)maxHealth);
	}
}